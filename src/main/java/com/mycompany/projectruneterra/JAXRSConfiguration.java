package com.mycompany.projectruneterra;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {

    @Override
    public Set<Class<?>> getClasses() {
    Set<Class<?>> set = new HashSet<>();
    set.add(com.mycompany.projectruneterra.rest.SummonerService.class);
    return set;
    }
}
