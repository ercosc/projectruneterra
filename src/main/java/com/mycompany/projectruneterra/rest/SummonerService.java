package com.mycompany.projectruneterra.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/summoner")
public class SummonerService {
    
    @GET
    public Response getSummoner() {
        return Response.ok().build();
    }
}
